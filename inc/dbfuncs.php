<?php
function dbconn() {
	require ('config.php');
	$conn = @new mysqli(db_host, db_user, db_pass, db_name);
	if (mysqli_connect_errno())
	{
		echo 'Error connecting to database.<br><br>The credentials entered were incorrect.  Please check the credentials and try again.';
		exit;
	}
	else
		return $conn;
}
?>
