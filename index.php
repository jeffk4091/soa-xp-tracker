<?php
session_start();
date_default_timezone_set('UTC');
include("./inc/templates.php");
require("./inc/dbfuncs.php");

$footer = template("footer");
$conn = dbconn();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Spirits of Arianwyn Skill Competitions</title>
	<script src="./template/js/sorttable.js"></script>
<?= $headerinclude = template("headerinclude");?>
</head>
<body id="competitions--home">
	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title">Currently Running Competitions</h3>
		<?php displayCompetitionData(1); ?>
		<h3 class="page-title">Upcoming  Competitions</h3>
		<?php displayCompetitionData(0); ?>
		<h3 class="page-title">Previous Competitions</h3>
		<?php displayCompetitionData(2); ?>
		<br>
	</section>

	<?= $footer; ?>

</body>
</html>

<?php 
function displayCompetitionData($status)
{
	global $conn;
	$errormsg = "";
	$query = "";
	switch ($status)
	{
		case 1:
			$query = "select * from competitions where (status = 1 or status = 3) and privacy = 0 order by compid desc";
			if(isset($_SESSION['is_auth']))
			{
				$query = "select * from competitions where status = 1 or status = 3 order by compid desc";
			}
			$errormsg = "No current competitions; check back soon!";
			break;
		case 0:
			$query = "select * from competitions where status = 0 and privacy = 0 order by starttime asc";
			if(isset($_SESSION['is_auth']))
			{
				$query = "select * from competitions where status = 0 order by starttime asc";
			}
			$errormsg = "No upcoming competitions; check back soon!";
			break;
		case 2:
			$query = "select * from competitions where status = 2 and privacy = 0 order by endtime desc";
			if(isset($_SESSION['is_auth']))
			{
				$query = "select * from competitions where status = 2 order by endtime desc";
			}
			if(!isset($_GET['viewall']))
			{
				$query = $query . " limit 0,10";
			}
			$errormsg = "No previous competitions; check back soon!";
			break;
		default:
			break;
			
	}
	
	$result = $conn->query($query);
	if($result->num_rows == 0)
	{
		echo $errormsg;
		return;
	}
	else
	{
		echo "<table class=\"sortable competition-table\"><tr class=\"competition-table__header\"><th class=\"table-header__item table-header--name\">Competition Name</th><th class=\"table-header__item table-header--skill\">Skill</th><th class=\"table-header__item table-header--starttime\">Start Time</th><th class=\"table-header__item table-header--endtime\">End Time</th></tr>";
		for ($i = 0; $i < $result->num_rows; $i++)
		{		
			$row = $result->fetch_assoc();
			$startdate = date('m/d/Y H:i', $row['starttime']);
			$enddate = date('m/d/Y H:i', $row['endtime']);
			$private = "";
			if($row['privacy'] == 1)
			{
				$private = " <acronym title=\"This competition is private; it is only visible in this view when logged in\">[Private]</acronym>";
			}
			echo "<tr class=\"competition-table__data\"><td class=\"competition-data__item data-item--name\"><a href=\"./viewcomp.php?compid=".$row["compid"]."\" class=\"competition-data__item-nameLink\">".$row["compname"]."</a>".$private."</td><td class=\"competition-data__item data-item--skill\">".$row["skill"]."</td><td class=\"competition-data__item data-item--starttime\">".$startdate."</td><td class=\"competition-data__item data-item--endtime\">".$enddate."</td></tr>";
		}
		echo "</table>";
	}
	if($status == 2 && !isset($_GET['viewall']))
	{
		echo "<a href=\"./index.php?viewall\" class=\"competition-data__item-nameLink\">View all previous competitions</a>";
	}
	
}
?>
