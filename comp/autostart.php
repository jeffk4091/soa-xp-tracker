<?php
chdir (dirname(dirname($argv[0]))); //change to project root. php5 safe
require("./inc/dbfuncs.php");

date_default_timezone_set('UTC');

$conn = dbconn();

$date = time();

$query="select * from competitions where status = 0";

$result = $conn->query($query);

if($result->num_rows > 0)
{
	for ($i = 0; $i < $result->num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		if($row['starttime'] < $date || $row['starttime'] < $date + 300)
		{
			startComp($row['compid']);
		}
	}
}

$query = "select * from competitions where status != 0";

$result = $conn->query($query);

if($result->num_rows > 0)
{
	for($i = 0; $i < $result->num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		if($row['endtime'] < $date && $row['status'] != 2)
		{
			endComp($row['compid']);
		}
	}
}

$conn->close();


function startComp($compid)
{
	$phploc = PHP_BINDIR;
	$action = "start";
	$command = "bash " . getcwd() . "/comp/runUpdate.sh " . $phploc . "/php " . $action . " " . $compid . " > /dev/null &";
	exec ( $command, $arrayout );
}

function endComp($compid)
{
	$phploc = PHP_BINDIR;
	$action = "end";
	$command = "bash " . getcwd() . "/comp/runUpdate.sh " . $phploc . "/php " . $action . " " . $compid . " > /dev/null &";
	exec ( $command, $arrayout );
}
?>