<?php
/* This script is to be executed as a command line script.
 * This script will be run in the background to gather and update stats for competitions
 * in a manner that is slow enough to not overload Jagex's limits
 */

set_time_limit(300); //5 minutes, though should never take this long

require(getcwd()."/inc/dbfuncs.php");
include(getcwd()."/comp/compapi.php");
date_default_timezone_set('UTC');

$action = null;
$compid = null;
$currtime = time();
$conn = dbconn();
$members = array();

if(count($argv) >= 3)
{
	$action = $argv[1];
	$compid = $argv[2];
	if(count($argv) > 3)
	for($i = 3; $i < count($argv); $i++)
	{
		array_push($members, $argv[$i]);
	}
	
	if ($action == "add")
	{
		startComp($conn, $compid);
		setUpdateTime($conn, $compid, time());
		setCompStatus($conn, 0);
		exit();
	}
	elseif ($action == "update")
	{
		setCompStatus($conn, 3);
		updateAllPlayers($conn, $compid);
		setUpdateTime($conn, $compid, time());
		setCompStatus($conn, 1);
 		exit();
	}
	elseif($action == "start")
	{
		setCompStatus($conn, 3);
		setUpdateTime($conn, $compid, time());
		startComp($conn, $compid);
		setUpdateTime($conn, $compid, time());
		setCompStatus($conn, 1);
		exit();
	}
	elseif($action == "edit")
	{
		editMembers($compid, $members);
		exit();
	}
	elseif($action == "end")
	{
		setCompStatus($conn, 3);
		setUpdateTime($conn, $compid, time());
		updateAllPlayers($conn, $compid);
		setCompStatus($conn, 2);
	}
	else {
		exit(); //if none of these actions, get outta here
	}
}
else {
	exit(); //if its wrong, then just do absolutely nothing for now
}

function setCompStatus($conn, $statuscode)
{
	global $compid, $currtime;
	$query = "update competitions set status = \"".$statuscode."\" where compid = \"".$compid."\"";
	$result = $conn->query($query);
	if(!$result)
	{
		return false;
	}
	else
		return true;
}

function editMembers($compid, $members)
{
	global $conn;
	$skill = getSkill($conn, $compid);
	for($i = 0; $i < count($members); $i ++) {
		$currentstats = getXpAndLevel ( $members[$i], $skill, true );
		$query = "select id from participants where compid='$compid' and player='$members[$i]'";
		$result = $conn->query($query);
		$playerrow = $result->fetch_assoc();

		echo "Member: ". $members[$i]. " ".$playerrow['id'];

		initializePlayer ( $conn, $playerrow ['id'], $currentstats [1], $currentstats [0] );
	}
}

?>