#!/bin/bash

if [[ $1 == *php* ]]
then
  command=$1
  shift
  cwd=$(pwd)
  $command -f $cwd/comp/getStats.php $@
fi

echo $?
