<?php
session_start ();
include ("./inc/templates.php");

if(isset($_GET['action']))
{
	$action = $_GET ['action'];
}
if (isset ( $action ) && $action == "loggedout") {
	session_destroy();
	$displaylogout = template ( "templogout" );
	echo $displaylogout;
} elseif (isset ( $action ) && $action == "logout") {
	// Put the logout script here!
	$logoutkey = $_GET ['logout'];
	if ($logoutkey == "dologout") {
		session_destroy ();
		header ( "LOCATION: ./login.php?action=loggedout" ); // redirect to the logout page when logged out
	} else {
		header ( "LOCATION: ./index.php" );
	}
} elseif (isset ( $action ) && $action == "dologin") {
	
	require ("./inc/dbfuncs.php");
	$conn = dbconn();
	
	$query = "select * from settings where setname = \"adminpass\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$temppass = mysqli_escape_string($conn, $_POST['password']);
	if (password_verify($temppass, $row['setval'])) {
		// is_auth is important here because we will test this to make sure they can view other pages
		// that are needing credentials.
		$_SESSION ['is_auth'] = true;
		
		$destpage = $_POST['dest'];
		
		if ($destpage == "login.php" || empty($destpage)) {
			$sendto = "index.php";
		}
		else {
			$params = $_POST['params'];
			if (isset($params) && !empty($params)) {
				$querystring = "?".$params;
			}
			else {
				$querystring = "";
			}
			$sendto = $destpage.$querystring;
		}
		
		
		header ( "LOCATION: ./".$sendto ); // redirect to the index when they log in.
		exit ();
	} else {
		$loginerror = template ( "temploginerror" );
		echo $loginerror;
	}
} else {
	session_destroy();
	$login = template ( "templogin" );
	echo $login;
}

?>


