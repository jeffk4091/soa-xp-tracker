<?php
date_default_timezone_set('UTC');

require("./inc/dbfuncs.php");
include("./comp/compapi.php");
include("./inc/templates.php");

$conn = dbconn( );

$cumxp = null;
$avgxp = null;
$cumlvl = null;
$avglvl = null;
$cumxpgained = null;
$avgxpgained = null;
$cumlvlgained = null;
$avglvlgained = null;
$compname = null;

if(!isset($_GET['compid']))
{
	$progress = false;
	$pagetitle = "Error Viewing Clan Competition";
}
else {
	$progress = true;
	$compid = $_GET['compid'];
	$compname = getCompName($conn, $compid);
	$pagetitle = "Viewing: ".$compname;
}


?>

<!DOCTYPE html>
<html>
<head>
<?= $headerinclude = template("headerinclude");?>
</head>
<body style="background-color: #181818;">

<h3 class="page-title"><?= $pagetitle; ?></h3>
<?php $skill = getSkill($conn, $compid); getData($conn, $compid); ?>
<p style="font-size: 12px; font-family: Tahoma, sans-serif;">Cumulative <?= $skill; ?> XP Gained: <?= $cumxpgained; ?>
<br>
Cumulative <?= $skill; ?> Levels Gained: <?= $cumlvlgained; ?><br>
Last Updated: <?php echo getLastUpdate($conn, $compid)." <acronym title=\"Game Time\">UTC</acronym>";?></p>

</body>
</html>

<?php 

function getLastUpdate($conn, $compid)
{
	$query = "select updatetime from competitions where compid = \"".$compid."\"";
	$result = $conn->query($query);
	if($result->num_rows == 1)
	{
		$row = $result->fetch_assoc();
		$updatetime = date('m/d/Y H:i ', $row['updatetime']);
		return $updatetime;
	}
	
}
function getData($conn, $compid)
{
	global $cumxp, $avgxp, $cumlvl, $avglvl, $cumxpgained, $avgxpgained, $cumlvlgained, $avglvlgained;
	
	$query = "select sum(endxp) as cumxp from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumxp = $row['cumxp'];
	}
	
	$query = "select sum(endlvl) as cumlvl from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumlvl = $row['cumlvl'];
	}
	
	$query = "select sum(xpgained) as cumxpgained from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumxpgained = $row['cumxpgained'];
	}
	
	$query = "select sum(lvlgained) as cumlvlgained from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumlvlgained = $row['cumlvlgained'];
	}
	
	$query = "select count(*) as total from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$count = $row['total'];
	}
	
	$avgxp = round($cumxp / $count);
	$avglvl = round($cumlvl / $count);
	$avgxpgained = round($cumxpgained / $count);
	$avglvlgained = round($cumlvlgained / $count);
	
	$cumxp = number_format($cumxp);
	$avgxp = number_format($avgxp);
	$cumlvl = number_format($cumlvl);
	$avglvl = number_format($avglvl);
	$cumxpgained = number_format($cumxpgained);
	$avgxpgained = number_format($avgxpgained);
	$cumlvlgained = number_format($cumlvlgained);
	$avglvlgained = number_format($avglvlgained);
}
?>