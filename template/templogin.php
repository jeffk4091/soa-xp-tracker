<?php
// include("../inc/templates.php");


$redirect = $_GET['dest'];
$queryparams = $_GET['params'];
?>



<!DOCTYPE html>
<html>
<head>
<title>Log In</title>
<?= $headerinclude = template("headerinclude");?>
</head>
<body id="competitions--login">
	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title">Sign In</h3>
		<form action="login.php?action=dologin" method="post" id="competition-login--form">
		<label>Please enter the administrator password to sign in.
		<input type="password" name="password" required autofocus>
		<input type="hidden" name="dest" value="<?= $redirect ?>">
		<input type="hidden" name="params" value="<?= $queryparams?>">
		<input type="submit" name="login" value="Login">
		</label>
		</form>
		</section>
			<?= $footer = template("footer"); ?>

</body>
</html>
		