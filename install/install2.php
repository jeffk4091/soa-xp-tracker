<html>
<head>
<title>SoA Competition Tracker - Database Setup</title>
</head>
<body>
<?php
error_reporting ( 0 );
// Grab information from submission
$db_host = $_POST ['dbhost'];
$db_user = $_POST ['dbuser'];
$db_pass = $_POST ['dbpass'];
$db_name = $_POST ['dbname'];
$admin_pw = $_POST ['adminpass'];
$admin_pw2 = $_POST ['adminpass2'];

if ($admin_pw != $admin_pw2) {
	echo "Admin Passwords do not match.  Try again.";
	exit ();
}

// Create database connection
$conn = @new mysqli ( $db_host, $db_user, $db_pass, $db_name );

if (mysqli_connect_errno ()) {
	echo 'Error connecting to database.<br><br>The credentials entered were incorrect.  Please check the credentials and try again.';
	echo '<br><a href="install.php">Return to Credential Enter page</a>';
	exit ();
}

// Create config file
$filecontents = '<?php
define(\'db_host\', \''.$db_host.'\');
define(\'db_user\', \''.$db_user.'\');
define(\'db_pass\', \''.$db_pass.'\');
define(\'db_name\', \''.$db_name.'\');
?>';

$cfile = fopen ( "../inc/config.php", "w" ); // wipe if exists
fwrite ( $cfile, $filecontents );
fclose ( $cfile );

/*
 * Create database tables:
 * Existing tables with the same name are dropped, and new tables are created. Some have populated data to start
 */
$query = "drop table if exists competitions";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( dcomp );
	exit ();
}

$query = "create table competitions(compid int unsigned not null auto_increment primary key, compname varchar(50) not null, skill varchar(30) not null, status int not null, starttime int unsigned not null, endtime int unsigned not null, updatetime int unsigned not null, privacy int unsigned not null)";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( ccomp );
	exit ();
}

$query = "drop table if exists participants";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( dparticipants );
	exit ();
}

$query = "create table participants(id int unsigned not null auto_increment primary key, player varchar(12) not null, compid int unsigned not null, startxp bigint unsigned not null, startlvl int unsigned not null, endxp bigint unsigned not null, endlvl int unsigned not null, xpgained bigint unsigned not null, lvlgained int unsigned not null )";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( cparticipants );
	exit ();
}

$query = "drop table if exists settings";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( dsettings );
	exit ();
}

$query = "create table settings(setname varchar(30) not null primary key, setval varchar(255))";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( csettings );
	exit ();
}

$passoptarray = [
		'cost' => 12,
];
$setnewpass = password_hash($admin_pw, PASSWORD_DEFAULT, $passoptarray);
$query = "insert into settings values('adminpass', '" . $setnewpass . "')";
$result = $conn->query ( $query );

if (! $result) {
	db_fail ( fsettings );
	exit ();
}

echo "<h1>SoA Competition Tracker Installer - Database Creation - Step 2</h1>";
$conn->close ();

/*
 * Check Configuration File
 * This section attempts to connect to the database with the new database configuration file, to verify that it was written correctly
 */

require ('../inc/config.php');
$conn = @new mysqli ( db_host, db_user, db_pass, db_name );
if (mysqli_connect_errno ()) {
	echo "Error connecting to database with generated configuration file.  Check to make sure the configuration file was created appropriately and run the installer again.";
	echo '<br><a href="install.php">Return to Credential Enter page</a>';
	exit ();
}

// Log success of database table setup
echo "All installed.  Have fun!<br><br>It is recommended that you chmod the config.php file back to 644 and delete the install folder from this site, as you won't need it anymore (and don't want anyone else running it).";
$conn->close ();

/*
 * db_fail
 * This function is called if there is an error in setting up the database. It will print out the relevant error statement and then quit
 * @param: Action that caused error
 */
function db_fail($error) {
	if ($error == dcomp) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Competitions Table'";
	} 

	else if ($error == ccomp) {
		echo "<br><br>Database Setup failed at step 'Create Competitions Table'";
	} 

	else if ($error == dparticipants) {
		echo "<br><br>Database Setup failed at step 'Drop Existing participants table'";
	} 

	else if ($error == cparticipants) {
		echo "<br><br>Database Setup failed at step 'Create participants Table'";
	} 

	else if ($error == dsettings) {
		echo "<br><br>Database Setup failed at step 'Drop Existing Settings table'";
	} 

	else if ($error == csettings) {
		echo "<br><br>Database Setup failed at step 'Create Settings Table'";
	} 

	else if ($error == fsettings) {
		echo "<br><br>Database Setup failed at step 'Fill Settings Table'";
	} 

	else {
		echo "<br><br>Unsure as to why we reached this point!"; // should never run
	}
}

?>
</body>
</html>
